﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Pendrive Livre")]
[assembly: AssemblyDescription("Software desenvolvido pela Bemol Soluções em Tecnologia, recomendável somente para eliminar virus que infecta o pendrive e transforma documentos e programas em atalho.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Bemol Soluções em Tecnologia")]
[assembly: AssemblyProduct("Pendrive Livre")]
[assembly: AssemblyCopyright("Copyright ©  2015 - Todos os Direitos Reservados")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8541f9ed-59bc-4b72-9a1b-0aa08386f4e6")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: NeutralResourcesLanguageAttribute("pt-BR")]
