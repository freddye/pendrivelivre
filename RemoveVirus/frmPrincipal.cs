﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using KnightsWarriorAutoupdater;
using System.Net;


namespace RemoveVirus
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

      private void Form1_Load(object sender, EventArgs e)
        {
            
        
            /*var drives = from drive in DriveInfo.GetDrives()
                         where drive.DriveType == DriveType.Removable
                         select drive;

            cmbDispositivos.DataSource = drives.ToList();*/
        
            
            //MessageBox.Show("Test" + cmbDispositivos.SelectedDrive);

        }

        private void cmbDispositivos_SelectedIndexChanged(object sender, EventArgs e)
        {
            //carregarDispositivos();
        }

       
            
        

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void btnLimparVirus_Click(object sender, EventArgs e)
        {
            try
            {
                //Exibe documentos e pastas ocultas
                string parametro;
                parametro = " /c attrib -h -r -s /s /d " + cmbDispositivos.SelectedDrive.Remove(2) + "\\*.*";
                Process p = new Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                p.StartInfo.FileName = "cmd.exe";
                p.StartInfo.Arguments = parametro;
                p.Start();
                p.Close();

                //Remove os atalhos do dispositivo
                string[] arquivos = Directory.GetFiles(cmbDispositivos.SelectedDrive.Remove(2) + "/");
                //criamos um array com todos os arquivos do dispositivo "F:/".
                foreach (string arquivo in arquivos)
                {
                    //se após a posição do ponto for ".lnk",deletaremos o arquivo,um por um.
                    if (arquivo.Substring(arquivo.IndexOf(".")) == ".lnk")
                    {
                        File.Delete(arquivo);
                    }
                }

                /*/Verifica se os arquivos estão na pasta .Trashes
                string folder = @"" + cmbDispositivos.SelectedDrive.Remove(2) + "\\Trashes"; //nome do diretorio
                //Se o diretório existir...
                if (Directory.Exists(folder))
                {
                    //string parametro;
                    parametro =  " robocopy " + cmbDispositivos.SelectedDrive.Remove(2) + "\\Trashes " + cmbDispositivos.SelectedDrive.Remove(2) + "\\ move /E";
                    p = new Process();
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.StartInfo.FileName = "cmd.exe";
                    p.StartInfo.Arguments = parametro;
                    p.Start();
                    p.Close();
                    

                    

                    // To move an entire directory. To programmatically modify or combine
                    // path strings, use the System.IO.Path class.
                    //System.IO.Directory.Move(@"C:\Users\Public\public\test\", @"C:\Users\Public\private");
                }*/



                MessageBox.Show("Dispositivo limpo com sucesso!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {

                MessageBox.Show("ERRO:" + ex,"Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            


            
           






        }

        private void sobreToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var frmSobre = new frmSobre();
            frmSobre.ShowDialog();
        }

        private void btnProtegerDispositivo_Click(object sender, EventArgs e)
        {
            try
            {   
                string folder = @"" + cmbDispositivos.SelectedDrive.Remove(2) + "\\Autorun.inf"; //nome do diretorio a ser criado
                //Se o diretório não existir...
                if (!Directory.Exists(folder))
                {
                    //Criamos um com o nome folder
                    Directory.CreateDirectory(folder);
                }

                //Oculta a pasta Autorun.inf
                string parametro;
                parametro = " /c attrib +r +s +a +h /s /d " + cmbDispositivos.SelectedDrive.Remove(2) + "\\Autorun.inf";
                Process p = new Process();
                p.StartInfo.FileName = "cmd.exe";
                p.StartInfo.Arguments = parametro;
                p.Start();
                p.Close();

                MessageBox.Show("Dispositivo protegido com sucesso!","Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {

                MessageBox.Show("ERRO:" + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }

        private void verificarAtualizaçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            #region check and download new version program
            bool bHasError = false;
            IAutoUpdater autoUpdater = new AutoUpdater();
            try
            {
                autoUpdater.Update();
            }
            catch (WebException exp)
            {
                MessageBox.Show("Can not find the specified resource");
                bHasError = true;
            }
            catch (XmlException exp)
            {
                bHasError = true;
                MessageBox.Show("Download the upgrade file error");
            }
            catch (NotSupportedException exp)
            {
                bHasError = true;
                MessageBox.Show("Upgrade address configuration error");
            }
            catch (ArgumentException exp)
            {
                bHasError = true;
                MessageBox.Show("Download the upgrade file error");
            }
            catch (Exception exp)
            {
                bHasError = true;
                MessageBox.Show("An error occurred during the upgrade process");
            }
            finally
            {
                if (bHasError == true)
                {
                    try
                    {
                        autoUpdater.RollBack();
                    }
                    catch (Exception)
                    {
                        //Log the message to your file or database
                    }
                }
            }
            #endregion
        }
    }
}
